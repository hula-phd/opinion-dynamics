from typing import Iterator

import numpy as np
from matplotlib import animation, pyplot as plt
from matplotlib.collections import PathCollection
from numpy.typing import NDArray

from opinion_dynamics.configuration.config import Config
from opinion_dynamics.simulation import Simulator


class Animator:
    """Animator class."""

    def __init__(self, config: Config):
        self.loop = config.loop
        self.animation_delay = config.animation_delay
        self.fig, self.ax = plt.subplots()
        self.scat: PathCollection | None = None
        self.animation: animation.FuncAnimation | None = None
        self.simulator = Simulator(config)
        self._information_sources_ids = [inf_src.id for inf_src in config.information_sources]
        self.num_color_divisions = 25
        self.color_histogram = np.linspace(-1, 1, num=self.num_color_divisions)
        self._setup_graph()

    def _setup_graph(
            self,
            title: str = 'Opinion Dynamics',
            xlabel: str = 'Topic A',
            ylabel: str = 'Topic B',
    ):
        self.ax.set_title(title)
        self.ax.set_xlabel(xlabel)
        self.ax.set_ylabel(ylabel)
        plt.grid(True)

    def setup_plot(self) -> tuple[PathCollection]:
        data = next(self.data_stream())
        self.scat = self.ax.scatter(
            data[:, 0],
            data[:, 1],
            c=data[:, 0],
            vmin=0,
            vmax=self.num_color_divisions + 1,
            cmap="jet",
            edgecolor="k",
        )
        self.ax.axis([-1, 1, -1, 1])
        return self.scat,

    def data_stream(self) -> Iterator[NDArray]:
        for result in self.simulator.simulate():
            yield np.vstack(
                [
                    np.array([list(opinions.values()) for index, opinions in result.items() if index not in self._information_sources_ids]),
                    np.array([list(opinions.values()) for index, opinions in result.items() if index in self._information_sources_ids]),
                ],
            )

    def update(self, stream: Iterator[NDArray]) -> tuple[PathCollection] | None:
        if self.scat:
            self.scat.set_offsets(stream)
            self.scat.set_array(
                np.append(
                    self._get_colormap(stream[:-len(self._information_sources_ids)]),
                    np.array([float(self.num_color_divisions + 1) for _ in self._information_sources_ids])
                ),
            )

            return self.scat,
        return None

    def _get_colormap_offset(self, x: float) -> int:
        return int(np.histogram([x], bins=self.color_histogram)[0].nonzero()[0])

    def _get_colormap(self, data: Iterator[NDArray]) -> NDArray:
        return np.array([self._get_colormap_offset(float(point[0])) + point[1] for point in data])

    def start(self) -> None:
        """Start animation."""
        self.animation = animation.FuncAnimation(
            self.fig,
            self.update,
            interval=self.animation_delay,
            frames=self.data_stream,  # noqa type mismatch
            init_func=self.setup_plot,
            blit=True,
            repeat=self.loop,
        )
