from itertools import chain
from typing import Iterator

import numpy as np  # noqa Unused import. numpy will be used by the input parameters
from sklearn.neighbors import NearestNeighbors

from opinion_dynamics.configuration.config import Config


class Simulator:

    """Simulator class."""

    def __init__(self, config: Config):
        self.config = config
        self.neighbors_tree = NearestNeighbors(radius=config.knn_radius)

    def simulate(self, precision: int = 2) -> Iterator[dict[str, dict[str, float]]]:
        for _ in range(self.config.num_epochs):
            self._update_nearest_neighbors()
            for agent in self.config.agents:
                neighbors = self.neighbors_tree.radius_neighbors([[agent.opinions[topic].sentiment() for topic in self.config.topics]], return_distance=False)[0]
                agent.learn(
                    neighbors=[self.config.agents[i] for i in neighbors],
                    learn_trust=self.config.learn_trust,
                    learn_neighbors=self.config.learn_neighbors,
                    learn_opinion=self.config.learn_opinion,
                    learning_type=self.config.learning_type,
                    neighbors_weight=self.config.neighbors_weight,
                    neighbor_learning_type=self.config.neighbor_learning_type,
                )
            # for inf_src in self.config.information_sources:
            #     inf_src.brownian_motion()
            yield {
                a.id: {
                    t.id: a.opinions[t].sentiment(precision=precision)
                    for t in self.config.topics
                }
                for a in chain(self.config.agents, self.config.information_sources)
            }

    def _update_nearest_neighbors(self) -> None:
        data = [
            [agent.opinions[topic].sentiment() for topic in self.config.topics] for agent in self.config.agents
        ]
        self.neighbors_tree.fit(data)
