from matplotlib import pyplot as plt

from opinion_dynamics.configuration.custom.corners import config
from opinion_dynamics.animation import Animator


def main():
    """Run animation with a specified config."""
    animator = Animator(config=config)
    animator.start()
    plt.show()


if __name__ == "__main__":
    main()
