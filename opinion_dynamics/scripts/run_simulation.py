from opinion_dynamics.configuration.custom.corners import config
from opinion_dynamics.simulation import Simulator


def main():
    """Run simulation with a specified config."""
    simulator = Simulator(config=config)
    results = list(simulator.simulate())
    print(f"Finished with {len(results)} results.")


if __name__ == "__main__":
    main()
