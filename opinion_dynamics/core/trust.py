from copy import deepcopy

import numpy as np
from numpy.typing import NDArray

from opinion_dynamics.base.agent import Agent
from opinion_dynamics.base.parameters import Parameter, Parameters
from opinion_dynamics.core.opinion import Opinions


class InformationSource(Agent):
    """Information source class."""

    def __init__(self, opinions: Opinions | None = None):
        """Initialize information source class.

        Parameters
        ----------
        opinions: Optional[Opinions]
            Opinions possessed by the information source
        """
        super().__init__()
        self.opinions = deepcopy(opinions) if opinions else Opinions()

    def brownian_motion(self) -> None:
        """Enable simple dynamic to the information source's opinions."""
        for topic in self.opinions.keys():
            self.opinions[topic].update(tuple([np.random.randint(0, 2)]))

    def learn(self) -> None:
        """Learn opinions of information source."""
        raise NotImplementedError()


class Trust(Parameter):
    """Trust class."""

    def __init__(self, trustee: InformationSource, prior: NDArray):
        """Initialize trust class.

        Parameters
        ----------
        trustee: InformationSource
            Trusted information source
        prior: NDArray
            Prior probability to the information source
        """
        super().__init__(parameter_type=trustee, prior=prior)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, InformationSource):
            raise NotImplementedError()
        return self.trustee.id == other.id

    def __getitem__(self, item):
        return self.trustee.opinions[item]

    @property
    def trustee(self) -> InformationSource:
        return self.type


class Trusts(Parameters):
    """Trusts class."""

    def __init__(self, trusts: list[Trust] | None = None):
        """Initialize trusts class.

        Parameters
        ----------
        trusts: Optional[List[Trust]]
            List of trusts
        """
        super().__init__(Trust, trusts)
