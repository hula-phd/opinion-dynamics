import numpy as np

from opinion_dynamics.core.opinion import Opinions
from opinion_dynamics.core.trust import Trust, Trusts, InformationSource


def test_trust():
    """Test trust."""
    trustee = InformationSource(opinions=Opinions())
    tr = Trust(trustee=trustee, prior=np.array([1, 2, 3]))
    assert tr.trustee == trustee
    np.testing.assert_array_equal(tr.prior, np.array([1, 2, 3]))


def test_trusts():
    """Test trusts."""
    assert Trusts()
