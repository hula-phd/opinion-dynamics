import re

from opinion_dynamics.core.opinion import Opinions
from opinion_dynamics.core.trust import InformationSource


def test_information_source():
    """Test information source."""
    class MockOpinions(Opinions):
        attribute = True

    inf_s = InformationSource(opinions=MockOpinions())
    assert inf_s.opinions.attribute  # noqa unresolved attribute
    assert re.match(r"^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$", inf_s.id)
