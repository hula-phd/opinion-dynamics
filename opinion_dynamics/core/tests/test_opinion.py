import re

import numpy as np

from opinion_dynamics.core.opinion import Topic, Opinion, Opinions


def test_topic():
    """Test topic."""
    t = Topic(name="test_name")
    assert t.name == "test_name"
    assert re.match(r"^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$", t.id)


def test_opinion():
    """Test opinion."""
    op = Opinion(topic=Topic(name="test_name"), prior=np.zeros(5), component_weight=np.ones((5, 1)))
    assert op.topic.name == "test_name"
    assert re.match(r"^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$", op.topic.id)


def test_opinions():
    """Test opinions."""
    assert Opinions()
