import numpy as np
import pytest

from opinion_dynamics.core import agent
from opinion_dynamics.core.agent import FocalAgent
from opinion_dynamics.core.opinion import Opinions, Topic, Opinion
from opinion_dynamics.core.trust import Trusts, Trust, InformationSource


@pytest.fixture()
def focal_agent():
    topic = Topic('topic1')
    trustee = InformationSource()
    trustee.opinions.add(Opinion(topic, prior=np.array([[1, 2], [3, 4]]), component_weight=np.ones((1, 2))))
    focal_agent = FocalAgent()
    focal_agent.opinions.add(Opinion(topic, prior=np.array([[1, 2], [3, 4]]), component_weight=np.ones((1, 2))))
    focal_agent.trusts.add(Trust(trustee, prior=np.array([[1, 2], [3, 4]])))
    return focal_agent, focal_agent.opinions, focal_agent.trusts


def test_focal_agent_init(focal_agent):
    assert isinstance(focal_agent[0].opinions, Opinions)
    assert isinstance(focal_agent[0].trusts, Trusts)
    assert focal_agent[0].learn(neighbors=[]) is None


def test_focal_agent_learn(mocker, focal_agent):
    trust_mock = mocker.patch.object(agent.FocalAgent, '_learn_trust')
    opinion_mock = mocker.patch.object(agent.FocalAgent, '_learn_opinion')
    neighbors_mock = mocker.patch.object(agent.FocalAgent, '_learn_neighbors')
    focal_agent[0].learn([])
    trust_mock.assert_called_once_with(learning_type="normalized")
    opinion_mock.assert_called_once_with(learning_type="normalized", learn_trust=True)
    neighbors_mock.assert_called_once_with([], weight=0.3, learning_type="normalized")

    focal_agent[0].learn([], learn_trust=False)
    focal_agent[0].learn([], learn_opinion=False)
    focal_agent[0].learn([], learn_neighbors=False)
    focal_agent[0].learn([], learn_trust=False, learn_opinion=False)
    focal_agent[0].learn([], learn_trust=False, learn_neighbors=False)
    focal_agent[0].learn([], learn_opinion=False, learn_neighbors=False)
    focal_agent[0].learn([], learn_trust=False, learn_neighbors=False)
    focal_agent[0].learn([], learn_trust=False, learn_opinion=False, learn_neighbors=False)


def test_agent_learn_opinion(focal_agent):
    focal_agent[0]._learn_opinion("argmax", True)
    focal_agent[0]._learn_opinion("argmax", False)
    focal_agent[0]._learn_opinion("normalized", True)
    focal_agent[0]._learn_opinion("normalized", False)

    with pytest.raises(ValueError):
        focal_agent[0]._learn_opinion("unknown", False)  # noqa type mismatch


def test_agent_learn_trust(focal_agent):
    focal_agent[0]._learn_trust("argmax")
    focal_agent[0]._learn_trust("normalized")

    with pytest.raises(ValueError):
        focal_agent[0]._learn_trust("unknown")  # noqa type mismatch
