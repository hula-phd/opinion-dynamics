import uuid

import numpy as np
from numpy.typing import NDArray

from opinion_dynamics.base.parameters import Parameter, Parameters


class Topic:
    """Topic class."""

    def __init__(self, name: str):
        self.id = str(uuid.uuid4())
        self.name = name


class Opinion(Parameter):
    """Opinion class."""

    def __init__(self, topic: Topic, prior: NDArray, component_weight: NDArray):
        super().__init__(parameter_type=topic, prior=prior)
        self.component_weight = component_weight

    @property
    def topic(self) -> Topic:
        return self.type

    @property
    def normalized_component_weight(self) -> NDArray:
        return self.component_weight / self.component_weight.sum(axis=0)

    def _sentiment(self) -> float:
        return float(np.dot(np.divide(np.dot(self.posterior, range(self.posterior.shape[1])), self.posterior.sum(axis=1)), self.normalized_component_weight))

    def sentiment(self, precision: int = 2) -> float:
        return np.round(self._sentiment() / (self.posterior.shape[1] - 1) * 2 - 1, decimals=precision)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Topic):
            raise NotImplementedError()
        return self.topic.id == other.id


class Opinions(Parameters):
    """Opinions class."""

    def __init__(self, opinions: list[Opinion] | None = None):
        super().__init__(Opinion, opinions)
