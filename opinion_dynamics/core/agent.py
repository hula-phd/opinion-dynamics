from __future__ import annotations

from copy import deepcopy
from typing import Literal

from opinion_dynamics.base.agent import Agent
from opinion_dynamics.core.opinion import Opinions
from opinion_dynamics.core.trust import Trusts


class FocalAgent(Agent):
    def __init__(self, opinions: Opinions | None = None, trusts: Trusts | None = None):
        super().__init__()
        self.opinions = deepcopy(opinions) if opinions else Opinions()
        self.trusts = deepcopy(trusts) if trusts else Trusts()

    def learn(
            self,
            neighbors: list[FocalAgent],
            learn_opinion: bool = True,
            learn_trust: bool = True,
            learn_neighbors: bool = True,
            neighbors_weight: float = 0.3,
            learning_type: Literal['normalized', 'argmax'] = "normalized",
            neighbor_learning_type: Literal['normalized', 'argmax'] = "normalized",
    ):
        """
        Learn method that encompasses all learning subsidiaries.
        :param neighbors: List of neighbouring agents
        :param learn_opinion: Learn opinion from information sources
        :param learn_trust: Learn trust to information sources
        :param learn_neighbors: Learn opinion from neighbouring agents
        :param neighbors_weight: Weight parameter affecting learning from neighbouring agents
        :param learning_type: Learning type can have values {"normalized", "argmax"}
        :param neighbor_learning_type: Neighbour learning type can have values {"normalized", "argmax"}
        """
        if learn_trust:
            self._learn_trust(learning_type=learning_type)
        if learn_opinion:
            self._learn_opinion(learning_type=learning_type, learn_trust=learn_trust)
        if learn_neighbors:
            self._learn_neighbors(neighbors, weight=neighbors_weight, learning_type=neighbor_learning_type)

    def _learn_opinion(self, learning_type: Literal['normalized', 'argmax'], learn_trust: bool) -> None:
        """

        Learning opinion from agent's information sources.
        :param learning_type:
        :param learn_trust:
        :return:
        """
        for inf_src, trust in self.trusts.items():
            for topic in self.opinions.keys():
                if learning_type == "argmax":
                    self.opinions[topic].update(inf_src.opinions[topic].arg_max(), weight=trust.point_estimate[0][0] if learn_trust else 1)
                elif learning_type == "normalized":
                    self.opinions[topic].normalized_update(inf_src.opinions[topic])
                else:
                    raise ValueError(learning_type)

    def _learn_trust(self, learning_type: Literal['argmax', 'normalized']) -> None:
        """
        Learning trust to agent's information sources.
        :param learning_type:
        """
        for inf_src, trust in self.trusts.items():
            if learning_type == "argmax":
                trust.update(trust.arg_max(), weight=sum([opinion.kld(inf_src.opinions[topic]) for topic, opinion in self.opinions.items()]))
            elif learning_type == "normalized":
                trust.normalized_update(trust)
            else:
                raise ValueError(learning_type)

    def _learn_neighbors(self, neighbors: list[FocalAgent], weight: float, learning_type: Literal['normalized', 'argmax']) -> None:
        """Learning opinion from agent's neighbouring agents.

        Parameters
        ----------
        neighbors: list[FocalAgent]
            List of neighbouring agents
        weight: float
            Weight parameter affecting learning from neighbouring agents
        learning_type: str
            Neighbour learning type can have values {"normalized", "argmax"}
        """
        for neighbor in neighbors:
            for topic in self.opinions.keys():
                if learning_type == "argmax":
                    self.opinions[topic].update(neighbor.opinions[topic].arg_max(), weight=weight)
                elif learning_type == "normalized":
                    self.opinions[topic].normalized_update(neighbor.opinions[topic])
                else:
                    raise ValueError(learning_type)
