from dataclasses import dataclass
from typing import Literal

from opinion_dynamics.core.agent import FocalAgent
from opinion_dynamics.core.opinion import Topic
from opinion_dynamics.core.trust import InformationSource


@dataclass
class Config:
    """Simulation configuration encompasses values of all its parameters. This should be defined by a user herself."""

    num_epochs: int
    num_topics: int
    topic_structure: tuple[int, int]
    num_agents: int
    num_inf_sources: int
    learn_opinion: bool
    learn_trust: bool
    learning_type: Literal['normalized', 'argmax']
    learn_neighbors: bool
    knn_radius: float
    neighbors_weight: float
    neighbor_learning_type: Literal['normalized', 'argmax']
    agents: list[FocalAgent]
    information_sources: list[InformationSource]
    topics: list[Topic]
    animation_delay: int
    loop: bool

    @property
    def component_structure(self) -> tuple[int, int]:
        """Component structure assign size of a component weight vector.

        Returns
        -------
        Tuple[int, int]:
            Component structure
        """
        return self.topic_structure[0], 1
