import random

import numpy as np
from numpy.random import randint

from opinion_dynamics.configuration.config import Config
from opinion_dynamics.core.agent import FocalAgent
from opinion_dynamics.core.opinion import Opinion, Topic
from opinion_dynamics.core.trust import InformationSource, Trust

# Prior opinion boundaries
LOW, HIGH = 1, 10

# Custom simulation configuration. Topics, focal agents and information sources are defined later on
config = Config(
    num_epochs=100,
    num_topics=2,
    topic_structure=(1, 2),
    num_agents=100,
    num_inf_sources=4,
    learn_opinion=True,
    learn_trust=True,
    learning_type="argmax",
    learn_neighbors=True,
    knn_radius=0.2,
    neighbors_weight=0.3,
    neighbor_learning_type="normalized",
    agents=[],
    information_sources=[],
    topics=[],
    animation_delay=100,
    loop=False,
)

# Configuration of topics
config.topics = [Topic(name=f'Topic-{i + 1}') for i in range(config.num_topics)]

# Configuration of focal agents and information sources
config.information_sources = [InformationSource() for _ in range(config.num_inf_sources)]
config.agents = [FocalAgent() for _ in range(config.num_agents)]

# Information sources' priors
inf_src_priors = [
    (np.array([[1, 10]]), np.array([[1, 10]])),
    (np.array([[10, 1]]), np.array([[1, 10]])),
    (np.array([[1, 10]]), np.array([[10, 1]])),
    (np.array([[10, 1]]), np.array([[10, 1]])),
]

# Generating opinion priors to the given topics for all information sources
for inf_src_num, inf_src in enumerate(config.information_sources):
    for topic_num, topic in enumerate(config.topics):
        inf_src.opinions.add(
            Opinion(
                topic=topic,
                prior=inf_src_priors[inf_src_num][topic_num],
                component_weight=np.ones(config.component_structure),
            ),
        )

# Generating opinion priors to the given topics for all focal agents (component weights are uniformly distributed and priors are randomly generated)
for agt in config.agents:
    for t in config.topics:
        agt.opinions.add(
            Opinion(
                topic=t,
                prior=randint(low=LOW, high=HIGH, size=config.topic_structure),
                component_weight=np.ones(config.component_structure),
            )
        )

    # Generating trust priors to the given information sources for all focal agents (priors are uniformly distributed)
    num_selected_inf_sources = random.choice(range(1, config.num_inf_sources + 1))
    for inf_src in random.sample(config.information_sources, num_selected_inf_sources):
        agt.trusts.add(Trust(trustee=inf_src, prior=np.ones(config.topic_structure)))
