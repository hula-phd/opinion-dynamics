import numpy as np
import pytest

from opinion_dynamics.base.parameters import Parameter, Parameters


@pytest.fixture()
def priors():
    return np.array([[5, 6, 7, 8], [4, 3, 2, 1]]), np.array([[5, 6, 7, 8], [1, 2, 3, 4]])


@pytest.fixture()
def parameter():
    prior = np.array([[1, 2, 3, 4], [5, 6, 7, 8]])
    return Parameter(MockParameterType, prior=prior), prior


class MockParameterType(Parameter):
    pass


class AnotherMockParameterType(Parameter):
    pass


def test_parameter_eq(parameter):
    assert parameter[0] == Parameter(MockParameterType, prior=parameter[1])

    another_arrays = [[5, 6, 7, 8], [1, 2, 3, 4]]
    another_prior = np.array(another_arrays)

    assert parameter[0] != Parameter(MockParameterType, prior=another_prior)


def test_parameter_attributes(parameter):
    def assert_getter_and_setter(param, attr, ars):
        setattr(param, attr, ars)
        np.testing.assert_array_equal(getattr(param, attr), ars)

    assert_getter_and_setter(parameter[0], 'prior', parameter[1])
    assert_getter_and_setter(parameter[0], 'posterior', parameter[1])
    assert parameter[0].shape == (2, 4)


def test_parameter_update(parameter):
    parameter[0].update((0, 0))
    np.testing.assert_array_equal(parameter[0].posterior, np.array([[2, 2, 3, 4], [6, 6, 7, 8]]))
    parameter[0].update((1, 2))
    np.testing.assert_array_equal(parameter[0].posterior, np.array([[2, 3, 3, 4], [6, 6, 8, 8]]))


def test_parameter_point_estimate(parameter):
    np.testing.assert_allclose(parameter[0].point_estimate, np.array(
        [
            [0.1, 0.2, 0.3, 0.4],
            [0.19230769, 0.23076923, 0.26923077, 0.30769231]
        ]
    ))


def test_parameter_arg_max(parameter):
    assert parameter[0].arg_max() == (3, 3)


def test_parameter_normalized_update(parameter):
    par = Parameter(MockParameterType, prior=np.array([[5, 6, 7, 8], [1, 2, 3, 4]]))
    parameter[0].normalized_update(par)
    np.testing.assert_allclose(parameter[0].posterior, np.array(
        [
            [1.19230769, 2.23076923, 3.26923077, 4.30769231],
            [5.1, 6.2, 7.3, 8.4]
        ]
    ))


def test_parameter_kld_update(parameter):
    par = Parameter(MockParameterType, prior=np.array([[5, 6, 7, 8], [1, 2, 3, 4]]))
    parameter[0].kld_update(par)
    np.testing.assert_allclose(parameter[0].posterior, np.array(
        [
            [-0.60943791, -0.19722458, 0.45810642, 1.22741128],
            [13.04718956, 12.59167373, 12.93108502, 13.54517744]
        ]
    ))


def test_parameter_kld(parameter, mocker):
    from opinion_dynamics.base import parameters
    mocker.patch.object(parameters, 'rel_entr', return_value=[-1])
    par = Parameter(MockParameterType, prior=np.array([[5, 6, 7, 8], [1, 2, 3, 4]]))
    assert parameter[0].kld(par) == 0

    mocker.patch.object(parameters, 'rel_entr', return_value=[4])
    par = Parameter(MockParameterType, prior=np.array([[5, 6, 7, 8], [1, 2, 3, 4]]))
    assert parameter[0].kld(par) == 3

    mocker.patch.object(parameters, 'rel_entr', return_value=[2])
    par = Parameter(MockParameterType, prior=np.array([[5, 6, 7, 8], [1, 2, 3, 4]]))
    assert parameter[0].kld(par) == 2


def test_parameters_init():
    parameters = Parameters(MockParameterType)
    assert parameters.parameters == {}


def test_parameters_getitem(priors):
    parameters = Parameters(MockParameterType)
    par = Parameter(MockParameterType, prior=priors[0])
    parameters.add(par)
    assert parameters[MockParameterType] == par  # noqa type mismatch


def test_parameters_get(priors):
    parameters = Parameters(MockParameterType, [Parameter(MockParameterType, prior=priors[0])])
    par = Parameter(MockParameterType, prior=priors[0])
    assert parameters.get(MockParameterType) == par


def test_parameters_iter(priors):
    parameters = Parameters(MockParameterType, [Parameter(MockParameterType, prior=priors[0])])
    par = Parameter(MockParameterType, prior=priors[0])
    for param in parameters:
        assert param == par


def test_parameters_setitem(priors):
    parameters = Parameters(MockParameterType, [Parameter(MockParameterType, prior=priors[0])])
    parameters[AnotherMockParameterType] = priors[1]  # noqa type mismatch
    assert list(parameters.keys()) == [MockParameterType, AnotherMockParameterType]


def test_parameters_values(priors):
    parameters = Parameters(MockParameterType, [Parameter(MockParameterType, prior=priors[0]), Parameter(AnotherMockParameterType, prior=priors[1])])
    values = list(parameters.values())
    assert values[0].type is MockParameterType
    assert values[1].type is AnotherMockParameterType


def test_parameters_items(priors):
    parameters = Parameters(MockParameterType, [Parameter(MockParameterType, prior=priors[0])])
    items = list(parameters.items())
    assert items[0][0] is MockParameterType
    assert items[0][1].type is MockParameterType


def test_parameters_remove(priors):
    parameters = Parameters(MockParameterType, [Parameter(MockParameterType, prior=priors[1])])
    parameters.remove(MockParameterType)
    assert len(parameters.parameters) == 0
