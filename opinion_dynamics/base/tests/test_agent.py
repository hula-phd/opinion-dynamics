import re

from opinion_dynamics.base.agent import Agent


def test_agent():
    """Test agent."""
    class MockAgent(Agent):
        def learn(self):
            return 'test_return'

    agent = MockAgent()
    assert re.match(r"^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$", agent.id)
    assert agent.learn() == 'test_return'
