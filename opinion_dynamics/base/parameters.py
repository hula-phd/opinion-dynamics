from __future__ import annotations

import uuid
from copy import deepcopy
from typing import Iterable, Any, Type, ValuesView, KeysView, ItemsView

import numpy as np
from numpy.typing import NDArray
from scipy.special import rel_entr


class Parameter:
    """Parameter class."""

    def __init__(self, parameter_type: Any, prior: NDArray):
        """Initialize parameter.

        Parameters
        ----------
        parameter_type: Any
            Parameter type
        prior: NDArray
            Prior probability
        """
        self.id = str(uuid.uuid4())
        self._prior = prior.astype(np.float_).copy()
        self._posterior = prior.astype(np.float_).copy()
        self.type = parameter_type

    def __eq__(self, other) -> bool:
        """Compare two distinct posteriors.

        Parameters
        ----------
        other

        Returns
        -------
        bool:
            Posterior element-wise comparison
        """
        return np.array_equal(self.posterior, other.posterior, equal_nan=True)

    @property
    def prior(self) -> NDArray:
        return self._prior

    @prior.setter
    def prior(self, value: NDArray) -> None:
        self._prior = value

    @property
    def posterior(self) -> NDArray:
        return self._posterior

    @posterior.setter
    def posterior(self, value: NDArray) -> None:
        self._posterior = value

    @property
    def point_estimate(self) -> NDArray:
        """Point estimate.

        Returns
        -------
        NDArray:
            Point estimate i.e. normalization of posterior parameter
        """
        return (self.posterior.T / self.posterior.sum(axis=1)).T

    @property
    def shape(self) -> tuple[int, ...]:
        return self._prior.shape

    def update(self, indices: tuple, weight: float = 1.) -> None:
        """Update posterior by newly observed incoming data

        Parameters
        ----------
        indices: Tuple
            Coordinates of the updated value
        weight: float, default=1.
            Weight parameter
        """
        for row, col in enumerate(indices):
            self.posterior[(row, col)] += weight

    def arg_max(self) -> tuple:
        """Arguments of the maxima.

        Returns
        -------
        Tuple:
            Coordinates of the posterior maximum i.e. coordinates of a max value from a corresponding occurrence table
        """
        return tuple(np.argmax(self.posterior, axis=1))

    def normalized_update(self, other: Parameter) -> None:
        """Normalize update using a point estimate increment to the corresponding coordinates of the posterior occurrence table

        Parameters
        ----------
        other: Parameter
            Updated parameter
        """
        self.posterior += other.point_estimate

    def kld_update(self, other: Parameter) -> None:
        """Kullback-Leibler divergence update using a relative entropy increment to the corresponding coordinates of the posterior occurrence table

        Parameters
        ----------
        other: Parameter
            Updated parameter
        """
        self.posterior += rel_entr(self.posterior, other.posterior)

    def kld(self, other: Parameter, offset: int = 3) -> float:
        """Compute Kullback-Leibler divergence of two distinct parameters. Relative entropy with offset

        Parameters
        ----------
        other: Parameter
            Other parameter
        offset: float
            Offset because of divergence
        Returns
        -------
        float:
            Kullback-Leibler divergence value of the two parameters
        """
        if (x := float(np.sum(rel_entr(self._posterior, other._posterior)))) < 0:  # noqa W0212
            return 0
        if x > offset:
            return offset
        return x


class Parameters:
    """Parameters class that emulates dict-like behavior."""

    def __init__(self, parameter_type: Type[Parameter], parameters: list[Parameter] | None = None):
        """Initialize parameters.

        Parameters
        ----------
        parameter_type: Type[Parameter]
            Parameter type
        parameters: Optional[List[Parameter]]
            List of parameters
        """
        self.parameters = deepcopy({parameter.type: parameter for parameter in parameters}) if parameters else {}
        self.parameter_type = parameter_type

    def __iter__(self) -> Iterable[Parameter]:
        yield from self.values()

    def __getitem__(self, item: Parameter) -> Parameter:
        return self.parameters[item]

    def __setitem__(self, key: Parameter, value: NDArray) -> None:
        self.parameters[key] = self.parameter_type(key, value)

    def add(self, parameter: Parameter) -> None:
        self.parameters[parameter.type] = parameter

    def keys(self) -> KeysView:
        return self.parameters.keys()

    def values(self) -> ValuesView:
        return self.parameters.values()

    def items(self) -> ItemsView:
        return self.parameters.items()

    def remove(self, key) -> None:
        del self.parameters[key]

    def get(self, *args, **kwargs) -> Parameter | None:
        return self.parameters.get(*args, **kwargs)
