import uuid
from abc import ABC, abstractmethod


class Agent(ABC):
    """Abstract agent class."""

    def __init__(self):
        """Initialize abstract agent class."""
        self.id = str(uuid.uuid4())

    @abstractmethod
    def learn(self, *args, **kwargs) -> None:
        """Learn agent inner parameters. This method has to be defined

        *args : tuple
            Additional arguments
        **kwargs : dict | None
            Extra arguments
        """
