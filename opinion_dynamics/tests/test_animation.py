import opinion_dynamics.animation
from opinion_dynamics.animation import Animator
from opinion_dynamics.configuration.config import Config


class MockSimulator:
    """Mock Simulator class."""

    def __init__(self, *args, **kwargs):  # noqa unused args kwargs
        self.information_sources = []


def test_setup_plot(mocker):
    """Test setup plot."""
    mock_config = Config(
        num_epochs=10,
        num_topics=2,
        topic_structure=(3, 2),
        num_agents=3,
        num_inf_sources=2,
        learn_opinion=True,
        learn_trust=True,
        learning_type="normalized",
        learn_neighbors=True,
        knn_radius=0.2,
        neighbors_weight=0.3,
        neighbor_learning_type="argmax",
        agents=[],
        information_sources=[],
        topics=[],
        animation_delay=100,
        loop=False,
    )

    mocker.patch.object(opinion_dynamics.animation, 'Simulator', MockSimulator)
    animator = Animator(config=mock_config)
    assert animator.simulator
