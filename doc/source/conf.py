project = 'opinion-dynamics'
copyright = '2021, UTIA'
author = 'UTIA'
extensions = ['numpydoc', 'sphinx.ext.autodoc']

templates_path = ['_templates']
exclude_patterns = []
html_theme = 'alabaster'
html_static_path = ['_static']
