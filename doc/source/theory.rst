Motivation
==========

Opinion Formation
-----------------

The global adoption of social networks and technology in general indicates a change in the process of opinion formation.
Social networks and digital media have created a powerful platform for information exchange.
Thus a user's opinion can be solely formed based on information and opinions shared by information sources and/or other users (followers, friends, etc.) of the network.
This created a possibility to purposefully influence users' opinions.
Misinformation and fake-news are believed to thrive in echo chambers which are homogeneous groups of agents with very similar opinions.
The spread of misinformation has an irrevocable effect on the political discourse.
One of the pivotal problems therein is to model the formation and evolution of opinions within a network.
This contribution reports the preliminary results on a developed simulation testbed for studying dynamics of opinion formation on an agent-centric basis.

Introduction
------------
Social networks and online media in general have had an irrevocable impact on opinion formation [1]_.
Nowadays, social networks facilitate a considerable part of human interaction and thus also greatly influence the opinion formation of its users.
A soaring amount of misinformation and fake content can significantly influence a user's opinion and lead to opinion polarisation and cause erroneous decisions [2]_.
They provide a fertile ground for opinion polarisation where opinions are shifted towards the extremes.
Particularly, *echo chambers* play a significant role in this polarisation of opinions.
In the past decades a lot of attention has been paid to modelling opinion dynamics in social networks as well as ways of its influence [3]_, [4]_, [5]_.
However so far, no satisfactory approach has been found.
This contribution reports on a work in progress that aims to reliably and efficiently model opinion formation allowing further identification of echo chambers and their influence on the users.

Agent-centric approach to opinion formation
-------------------------------------------
In the context of social nets **opinion formation** can be regarded as a mechanism how the agent combines different sources of information to form opinion about a particular topic.
Opinion formation depends on trust the agent assign to other agents as the influence of a trusted individual is much higher and his opinion has higher impact. So the agent weights others' opinions by trust value associated with the source providing this opinion.
In many cases topic can be characterised by a number of **features** representing specific details of the topic.
For instance when considering buying a computer the user may consider values of CPU, RAM, screen size, weight, price, and many other factors.
Each of the features may be rated by the user in the range from absolutely positive value (:math:`1`) down to an absolutely negative (:math:`0`) value. Note that sub-selection of features is fully determined by the user and it's goal, and indirectly reflects the user's DM preferences in he subsequent DM task (for instance which computer to buy).

Let us summarise the introduced notions.
The user of a social network is modelled as an **agent** that carries out decisions.
These agents hold **opinions** on **topics** and assign **trust** to other agents.
A topic can be any entity, for instance an organisation, event, person, product/service, etc. characterised by the set of features selected by the agent.

Consider a topic :math:`\tau \in \mathcal{T}` with :math:`m\in\mathbb{N}` features.
Let us assume that the agents attitude to each feature is expressed by a probability mass function on previously selected :math:`n \in \mathbb{N}` discrete values.
The assumption is not limiting as we can consider a sufficiently large range of values.
For instance :math:`n=5` covers a quantification broadly used in sentiment analysis (strong positive, positive, neutral, negative, strong negative).
Let the **opinion** :math:`\mathbf{O}^{\tau}\in\mathbb{R}^{m,n}` on a topic :math:`\tau` be represented by a real normalised matrix

.. math:: \mathbf{O} = \begin{pmatrix}
                         \label{eq:opinion_matrix}
                         o_{1,1} & \ldots & o_{1, n} \\
                         \vdots  & \ddots & \vdots   \\
                         o_{m,1} & \ldots & o_{m, n} \\
          \end{pmatrix}.

In this equation the :math:`i-`th row :math:`o_{i,\bullet}= \left(o_{i,1},\cdots,o_{i,n}\right)` represents the agent's attitude towards the :math:`i-$th` feature, which is modelled as a discrete random variable whose probability mass function represented by the row :math:`o_{i,\bullet}`.
    This is designed to be compatible with the way opinion data are usually gathered.
    When one gathers postings of agents from a social network e.g. tweets from Twitter, it is possible to assign a sentiment value to the posting that represents the the (dis)agreement expressed in the posting towards a topic.
    Gathering several postings gives a set of sentiments from an agent towards a topic.
    This set then corresponds to a row of the opinion matrix :math:`\mathbf{O}`.

Example 1
_________
Consider and illustrative example where an agent, *Alice*, aims to form an opinion on a specific **topic**, for instance a *Political party*.
Her DM is to vote the party which reflects Alice's priorities and needs in the best way.
In order to do that and elaborate them while considering her own priorities, she gathers information from available information sources and other agents.
**Information sources**  are special types of agents which provide opinions on a larger scale, for instance mass media, blogs, etc.
*Alice* considers a newspaper with relevant information on the *Political party* and updates her opinion on the *Political party* considering the opinion of the newspaper.
*Alice* can also update her opinion based on the opinions of her peers. She may select agents whom she trusts and updates her opinion based on theirs.
The party programmes in different aspects (*healthcare, education, social politics, taxes, childcare, ecology, pension,...*) form the whole set of features. Alice determines subset of features that reflects her DM priorities.
For instance if alice is a student will consider a subset of features like *healthcare*, *education*, *taxes* while a agent-senior will more likely select *healthcare*, *pension*.
Thus an opinion of a voter (Alice) will then be formed based on a subset of selected features and values assigned to them.

Opinion formation algorithm
___________________________
The proposed algorithm of the simulation progresses as follows.
As a first steps, the topics are initialized.
Then information sources are created with a chosen prior opinion configuration.
Subsequently all agents are initialized with a chosen prior opinion.
Additionally, each agent gets assigned its information sources.

After the initialization, the main iteration loop is started.
Since all agents will learn from their neighbors, a k-nearest neighbors (kNN) tree of agents is recalculated.
The euclidean metric in the opinion space is used to construct the nearest neighbor tree.

Then, each agent performs an update of its own opinion on all topics based on the opinions of both information sources, and of their neighbors inside a predefined kNN radius.

The learning from information sources is straight forward.
The random assignment of a subset of information sources to agents at the initialization phase is intended to simulate the preferences of an agent.
The opinions of all information sources assigned to an agent are hence considered and updated from.
The update of opinion :math:`\mathbf{o}^A_{i,\bullet}` of an agent :math:`A` on a subtopic :math:`\tau_i` is done by taking the row :math:`\mathbf{o}_{i,\bullet}` of :math:`\mathbf{O}`, normalizing it by

.. math:: \mathbf{o}^{B,\text{NORM}}_{i,j}= \frac{\mathbf{o}^B_{i,j}}{\sum_k \mathbf{o}^B_{i,k}},

the opinions are then updated as

.. math:: \mathbf{o}^A_{i,\bullet} = \mathbf{o}^A_{i,\bullet} + \mathbf{o}^{B,\text{NORM}}_{i,\bullet}.

The learning from the agents neighbors procedes in a similar fashion.
The neighbors in a given radius are queried for updates.
The updating is done similarly with an additional factor of a *trust* vector which represents the value of the trust an agent holds towards a neighbor.
The elements of the trust vector :math:`\mathbf{t}^{A,B}\in\mathbb{R}^{1,m}` represent the weight with which agent :math:`A` trusts agent :math:`B` in each subtopic.
In this example, the trust values are fixed, however they can also undergo learning.

References
----------
.. [1] W. Quattrociocchi, G. Caldarelli, and A. Scala. *Opinion dynamics on interacting networks: media competition and social influence*. Scientific Reports **4** (2014), 4938.
.. [2] B. Collins, D. T. Hoang, N. T. Nguyen, and D. Hwang. *Trends in combating fake news on social media – a survey*. Journal of Information and Telecommunication **5** (2021), 247–266.
.. [3] F. Baumann, P. Lorenz-Spreen, I. Sokolov, and M. Starnini. *Modeling echo chambers and polarization dynamics in social networks*. Physical Review Letters (01 2020).
.. [4] W. Cota, S. C. Ferreira, R. Pastor-Satorras, and M. Starnini. *Quantifying echo chamber effects in information spreading over political communication networks*. EPJ Data Science **8** (2019), 35.
.. [5] R. Ureña, G. Kou, Y. Dong, F. Chiclana, and E. Herrera-Viedma. *A review on trust propagation and opinion dynamics in social networks and group decision making frameworks*. Information Sciences **478** (2019), 461–475.
