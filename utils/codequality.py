import json
from hashlib import sha256
from pathlib import Path

from pylama.main import check_paths, parse_options


def get_severity(source: str) -> str:
    if source == 'mypy':
        return 'major'
    return 'minor'


def main() -> None:
    paths = ['../opinion_dynamics', '../utils']
    options = parse_options(paths, rootdir=Path(__file__).parents[1])
    print("Running linters")
    errors = check_paths(paths, options=options, rootdir=Path(__file__).parent)
    findings = [
        {
            'description': f'{("[" + error.number + "] ") if error.number else ""}{error.message} [{error.source}]',
            'fingerprint': sha256((str(index) + error.number + error.message + error.filename + str(error.lnum)).encode('utf-8')).hexdigest(),
            'severity': get_severity(error.source),
            'location': {
                'path': error.filename,
                'positions': {'begin': {'line': error.lnum}},
            },
        }
        for index, error in enumerate(errors)
    ]
    output_path = Path(__file__).parents[1] / 'gl-code-quality-report.json'
    with open(output_path, 'w') as f:
        json.dump(findings, f, indent=' ')


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(f"Could not create the code quality report due to {e}")
