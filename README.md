# Opinion formation

A simulation library for opinion dynamics.  
The simulation consists of agents in an environment with information sources.  
Information sources disseminate knowledge to agents, which hold opinions on topics.
Agents modify their opinions based on the opinions of information sources to which they ascribe and based on the interaction between individual agents.

[[_TOC_]]

## Motivation

The global adoption of social networks and technology in general indicates a change in the process of opinion formation. Social networks and digital media have created a powerful platform for
information exchange. Thus a user’s opinion can be solely formed based on information and opinions shared by information sources and/or other users (followers, friends, etc.) of the network. This
created a possibility to purposefully influence users’ opinions. Misinformation and fake-news are believed to thrive in echo chambers which are homogeneous groups of agents with very similar opinions.
The spread of misinformation has an irrevocable effect on the political discourse. One of the pivotal problems therein is to model the formation and evolution of opinions within a network. This
contribution reports the preliminary results on a developed simulation testbed for studying dynamics of opinion formation on an agent-centric basis.
Further details can be found [here](https://gitlab.com/hula-phd/opinion-dynamics/-/blob/master/doc/writeup.pdf).

## Installation

[PyCharm](https://www.jetbrains.com/pycharm/) is the recommended IDE for python development, although any development environment might be used.

### Clone the project

Start by installing [git](https://git-scm.com/download) if you havent already.
Open a terminal and clone this repository to a desired location

```bash
git clone https://gitlab.com/hula-phd/opinion-dynamics.git
cd opinion-dynamics
```

### Install python

The project is written for python 3.10 which can be installed from the [official website](https://www.python.org/downloads/release/python-3109/).
Optionally, it is recommended for linux and macos systems to use `pyenv` for python version management although this is completely optional.

### Project dependencies

The project uses [poetry](https://python-poetry.org/) (version 1.3.1) to manage python dependencies.
The easiest option is to install `poetry` into the system-wide python interpreter.  
First, verify that `pip` refers to python 3.10 by running

```bash
pip --version
```

and verifying that the output contains `Python 3.10`.
Otherwise, you may try the command `pip3`.  
Then install poetry

```bash
pip install --upgrade pip poetry==1.3.1
```

Verify that poetry is available by running

```bash
poetry --version
```

which should return `1.3.1`.  
For different poetry installation methods refer to the [official documentation](https://python-poetry.org/docs/#installation).

### Virtual environment

It is recommended to create a virtual environment with the project dependency packages to separate them from system packages.
Poetry creates virtual environments by default.
It is recommended to put the virtual environment folder into the project folder, do so by setting

```bash
poetry config virtualens.in-project true
```

### Install dependencies

Run

```shell
poetry install
```

to create a virtual environment and install dependencies.
You may also install only essential dependencies without testing and code quality packages by `poetry install --only main`.

### Without poetry

The project can be used without poetry, the dependencies have to be resolved some other way though.  
The current dependencies can be found in the `pyproject.toml` file under the `[tool.poetry.dependencies]` key (the core ones being: `numpy`, `scikit-learn` and `matplotlib` as of now).

## Usage

The module `opinion_dynamics/scripts/run_animation.py` contains a predefined simulation with an animation.
Run it by executing

```bash
poetry run python opinion_dynamics/scripts/run_animation.py
```

This script demonstrates the basic usage of the `opinion_dynamics.animation.Animator` class

```python
from matplotlib import pyplot as plt

from opinion_dynamics.configuration.custom.corners import config
from opinion_dynamics.animation import Animator

animator = Animator(config=config)
animator.start()
plt.show()
```

Notice the `config` object imported from  `opinion_dynamics.configuration.custom.corners`.
It is a `opinion_dynamics.configuration.config.Config` object which is used for configuring the parameters of the simulation.
This is the main entrypoint when running custom simulations.
Users must instantiate a `Config` class for their simulations.

### Examining a configuration

Let us examine one of the predefined configurations to gain a better understanding of the `Config` class.
Consider the `opinion_dynamics/configuration/custom/corners.py` file.  
First, the basic `Config` is instantiated

```python
from opinion_dynamics.configuration.config import Config

config = Config(
    num_epochs=100,
    num_topics=2,
    topic_structure=(1, 2),
    num_agents=100,
    num_inf_sources=4,
    learn_opinion=True,
    learn_trust=True,
    learning_type="argmax",
    learn_neighbors=True,
    knn_radius=0.2,
    neighbors_weight=0.3,
    neighbor_learning_type="normalized",
    agents=[],
    information_sources=[],
    topics=[],
    animation_delay=100,
    loop=False,
)
```

with basic parameters:

- `num_epochs` Number of simulation time steps.
- `num_topics` The number of topics to simulate. (Boils down to the dimension of the animation.)
- `topic_structure` The shape of the topics. A tuple where the first number represents the number of subtopics and the second the number of topics.
- `num_agents` Number of agents to simulate.
- `num_inf_sources` Number of information sources. (Static agents that distribute opinions, marked by dark red in the animation.)
- `learn_opinion` Whether to simulate opinion dynamics.
- `learn_trust` Whether to simulate trust dynamics.
- `learn_neighbors` Whether to update opinions for neighbouring agents.
- `learning_type` The type of learning function to use. Valid values are `normalized` and `argmax`. Further metrics can be implemented in `opinion_dynamics/core/agent.py` in
  the `FocalAgent._learn_trust()`, `FocalAgent._learn_opinion()`, and `FocalAgent._learn_neighbors()` methods. See below for more details.
- `knn_radius` Radius in the topics space from which to learn neighbouring opinions. Agents will consider the opinions of all other agents in a ball with this radius.
- `neighbors_weight` A weight for the opinion of neighbours.
- `neighbor_learning_type` As `learning_type` but for the `neighbours`.
- `agents` A list of agents. (Filled later.)
- `information_sources` A list of information sources. (Filled later.)
- `topics` A list of topics. (Filled later.)
- `animation_delay` A delay between animation frames.
- `loop` Whether to loop the animation.

Notice the three empty lists of entities.
These are generated below.

#### Topics

Instatiate `opinion_dynamics.core.opinion.Topic` classes with names for each topic.

```python
config.topics = [Topic(name=f'Topic-{i + 1}') for i in range(config.num_topics)]
```

generates the appropriate topic number, in this case `config.topics = [Topic(name='Topic-1}'), Topic(name='Topic-2}')]`.

#### Information sources

Instantiate empty information sources

```python
config.information_sources = [InformationSource() for _ in range(config.num_inf_sources)]
```

in this case equivalent to `config.information_sources = [InformationSource(), InformationSource(), InformationSource(), InformationSource()]`

#### Agents

Instantiate empty agents

```python
config.agents = [FocalAgent() for _ in range(config.num_agents)]
```

in this case equivalent to `config.agents = [FocalAgent(), FocalAgent(), .., FocalAgent()]`

#### Information source prior

Generate prior distributions for the information sources

```python
inf_src_priors = [
    (np.array([[1, 10]]), np.array([[1, 10]])),
    (np.array([[10, 1]]), np.array([[1, 10]])),
    (np.array([[1, 10]]), np.array([[10, 1]])),
    (np.array([[10, 1]]), np.array([[10, 1]])),
]
```

each row is a tuple representing an information source.
Each element of the tuple corresponds to a prior distribution for each topic.
E.g. The second information source has the prior opinion on `Topic-1` set to `np.array([[10, 1]])` and on `Topic-2` to `np.array([[1, 10]])`.  
These prior distributions are then set to the empty information sources in the `config`

```python
for inf_src_num, inf_src in enumerate(config.information_sources):
    for topic_num, topic in enumerate(config.topics):
        inf_src.opinions.add(
            Opinion(
                topic=topic,
                prior=inf_src_priors[inf_src_num][topic_num],
                component_weight=np.ones(config.component_structure),
            ),
        )
```

#### Agent prior opinions

Finally, the agents have their prior distributions randomized (`prior=randint(low=LOW, high=HIGH, size=config.topic_structure)`) and a random subset of the information sources are
selected (`random.sample(config.information_sources, num_selected_inf_sources)`).

```python
for agt in config.agents:
    for t in config.topics:
        agt.opinions.add(
            Opinion(
                topic=t,
                prior=randint(low=LOW, high=HIGH, size=config.topic_structure),
                component_weight=np.ones(config.component_structure),
            )
        )

    # Generating trust priors to the given information sources for all focal agents (priors are uniformly distributed)
    num_selected_inf_sources = random.choice(range(1, config.num_inf_sources + 1))
    for inf_src in random.sample(config.information_sources, num_selected_inf_sources):
        agt.trusts.add(Trust(trustee=inf_src, prior=np.ones(config.topic_structure)))

```

Note that this configuration is just an example.
The way you populate `information_sources`, `topics`, `opinions` and `agents` is largely irrelevant as long as the configuration specification is honoured.
In particular, one needs to to set the `topics`, `information_sources` and `agents` parameters as lists of `Topic`, `InformationSource` and `Agent` respectively.

### Data only

The animator class is useful for visualisation of simulations in two-dimensional spaces.
In a realistic example one needs higher dimensions and more datapoints both of which are hindered by the bulky `matplotlib` visualisation.
The animator class is a wrapper of `opinion_dynamics.simulation.Simulator` class that provides the functionality to run simulations.  
Use it to run when only the data are needed without the visualisation.
Example usage:

```python
from opinion_dynamics.configuration.custom.corners import config
from opinion_dynamics.simulation import Simulator

simulator = Simulator(config=config)
results = list(simulator.simulate())
print(results)
```

Running the predefined `corners.py` and `corners_with_neighbours.py` configurations results in

### Corners

![Corners gif](doc/images/corners.gif)

### Neighbours

![Neighbours gif](doc/images/neighbours.gif)

## Test

This project uses [pytest](https://docs.pytest.org/en/6.2.x/) as the testing framework.  
To run `pytest` with code coverage

```shell
 poetry run pytest --cov-branch --cov-report term-missing --cov=opinion_dynamics opinion_dynamics 
```

To run a code quality checker run

```shell
poetry run pylama opinion_dynamics
```

## License

The code is published under the MIT.

## Contribution

Everyone is more than welcome to contribute to this project by focusing on (not only) the following topics.

To contribute to the project, submit a [merge request](https://gitlab.com/hula-phd/opinion-dynamics/-/merge_requests) to this repository. The contribution must pass the tests and must not raise any
code quality issues

### Learning methods

Existing learning methods are `arg_max` and `normalized`.
A Plethora of other methods, e.g. fuzzy-like methods, could be implemented and deployed by the agents and/or information sources.
To implement a new learning method, implement a new method in `opinion_dynamics/base/parameters.py`.
The `Parameter` class (a parent of `Trust` and `Opinion`) has two methods for updating their internal state `update()` and `update_normalized()`.
See `opinion_dynamics/core/agent.py`, in particular the methods `_learn_opinion()` and `_learn_trust()` which call the aforementioned update methods.
Implement a new update method in the `Parameter` class and a new of-branch in the `Agent` class.  
Note the unfinished `kld_update`.

### Information sources dynamics

Until now, information sources could only be static as far as their internal parameters are concerned.
They can exhibit limited dynamic behaviour as they employ "brownian motion".
It would be nice to make these information sources truly dynamic by using some of the learning methods.

The `InformationSource` class in `opinion_dynamics/core/trust.py` possesses a `learn` method

```python
def learn(self) -> None:
    """Learn opinions of information source."""
    raise NotImplementedError()
```

This method is not implemented.

Instead, "brownian motion" can be simulated now but is turned off (see `opinion_dynamics/simulation.py` the `simulate()` method)

```python
# for inf_src in self.config.information_sources:
#     inf_src.brownian_motion()
```

and `opinion_dynamics/core/trust.py`

```python
def brownian_motion(self) -> None:
    """Enable simple dynamic to the information source's opinions."""
    for topic in self.opinions.keys():
        self.opinions[topic].update(tuple([np.random.randint(0, 2)]))
```

### Continuous development

The whole project is subject to change.
Literally,every line of code and corresponding theory should be upgraded and fine-tuned together.
There are presumably many hidden bugs, imperfections and bad design choices.
The best practice is to open an issue in GitLab and create a merge request with your code changes.
It can then be reviewed and merged to the master branch.
If you are uncertain, stuck, or do not understand how things work, please ask for help.
Do not hesitate and commit to this endeavour!
